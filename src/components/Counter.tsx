import { useContext, useState } from 'react'
import CounterContext, {
  useDispatch,
  useSelector,
  useStore,
} from '../store/store'

function Counter() {
  // const { state, inc } = useContext(CounterContext)
  // const { state, inc } = useStore()
  const { inc } = useStore()
  const state = useSelector((rootState: any) => rootState.count)
  const dispatch = useDispatch()
  return (
    <button className="Counter" onClick={() => dispatch(inc)}>
      {state}
    </button>
  )
}
export default Counter
