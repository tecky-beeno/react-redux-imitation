import { createContext, useContext, useState } from 'react'

const CounterContext = createContext<any>(null)

export default CounterContext

export const Provider = CounterContext.Provider

export const useStore = () => useContext(CounterContext)

export const useSelector = (selector: any) => selector(useStore().state)

export const useDispatch = () => (action: any) => action()

export const useCreateStore = (reducer: any) =>
  useState(() => reducer(undefined, { type: '@@INIT/1.x' }))
