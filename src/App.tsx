import React, { useState } from 'react'
import logo from './logo.svg'
import './App.css'
import Counter from './components/Counter'
import Group from './components/Group'
import CounterContext, { Provider, useCreateStore } from './store/store'

function App() {
  const initialState = { count: 1 }
  const rootReducer = (state: any = initialState, action: any) => {
    switch (action.type) {
      case 'inc':
        return {
          ...state,
          count: state.count + 1,
        }
      default:
        return state
    }
  }
  const [state, setState] = useCreateStore(rootReducer)
  const inc = () =>
    setState((state: any) => rootReducer(state, { type: 'inc' }))
  const props = { inc, state }
  return (
    <div className="App">
      <header className="App-header">
        {/* <Counter {...props} /> */}
        {/* <Counter {...props} /> */}
        {/* <Group {...props} /> */}
        {/* <Group {...props} /> */}

        <Provider value={props}>
          <Group />
          <Group />
        </Provider>
      </header>
    </div>
  )
}

export default App
